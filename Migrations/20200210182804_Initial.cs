﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace kennings_aspdotnet.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Boards",
                columns: table => new
                {
                    BoardId = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Type = table.Column<string>(maxLength: 100, nullable: false),
                    Width = table.Column<int>(nullable: false),
                    Price = table.Column<double>(type: "Money", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Boards", x => x.BoardId);
                });

            migrationBuilder.CreateTable(
                name: "Joists",
                columns: table => new
                {
                    JoistId = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Type = table.Column<string>(maxLength: 100, nullable: false),
                    Price = table.Column<double>(type: "Money", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Joists", x => x.JoistId);
                });

            migrationBuilder.CreateTable(
                name: "Lengths",
                columns: table => new
                {
                    LengthId = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Value = table.Column<double>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Lengths", x => x.LengthId);
                });

            migrationBuilder.CreateTable(
                name: "BoardLength",
                columns: table => new
                {
                    BoardId = table.Column<int>(nullable: false),
                    LengthId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BoardLength", x => new { x.BoardId, x.LengthId });
                    table.ForeignKey(
                        name: "FK_BoardLength_Boards_BoardId",
                        column: x => x.BoardId,
                        principalTable: "Boards",
                        principalColumn: "BoardId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_BoardLength_Lengths_LengthId",
                        column: x => x.LengthId,
                        principalTable: "Lengths",
                        principalColumn: "LengthId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "JoistLength",
                columns: table => new
                {
                    JoistId = table.Column<int>(nullable: false),
                    LengthId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_JoistLength", x => new { x.JoistId, x.LengthId });
                    table.ForeignKey(
                        name: "FK_JoistLength_Joists_JoistId",
                        column: x => x.JoistId,
                        principalTable: "Joists",
                        principalColumn: "JoistId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_JoistLength_Lengths_LengthId",
                        column: x => x.LengthId,
                        principalTable: "Lengths",
                        principalColumn: "LengthId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Boards",
                columns: new[] { "BoardId", "Price", "Type", "Width" },
                values: new object[] { 1, 1.1699999999999999, "Discount Decking (84mm x 19mm)", 84 });

            migrationBuilder.InsertData(
                table: "Boards",
                columns: new[] { "BoardId", "Price", "Type", "Width" },
                values: new object[] { 2, 4.8700000000000001, "Smooth Hardwood Balau Decking (90mm x 19mm)", 90 });

            migrationBuilder.InsertData(
                table: "Boards",
                columns: new[] { "BoardId", "Price", "Type", "Width" },
                values: new object[] { 3, 4.8899999999999997, "Dark Brown Larch Brushwood Finish Decking Board (145mm x 38mm)", 145 });

            migrationBuilder.InsertData(
                table: "Boards",
                columns: new[] { "BoardId", "Price", "Type", "Width" },
                values: new object[] { 4, 1.74, "Standard Redwood Decking (120mm x 28mm)", 120 });

            migrationBuilder.InsertData(
                table: "Boards",
                columns: new[] { "BoardId", "Price", "Type", "Width" },
                values: new object[] { 5, 3.5099999999999998, "Brown - Standard Redwood Decking (120mm x 28mm)", 120 });

            migrationBuilder.InsertData(
                table: "Joists",
                columns: new[] { "JoistId", "Price", "Type" },
                values: new object[] { 1, 2.46, "Treated & Graded Decking Joist (3 1/2\" x 1 1/2\")" });

            migrationBuilder.InsertData(
                table: "Joists",
                columns: new[] { "JoistId", "Price", "Type" },
                values: new object[] { 2, 2.1499999999999999, "Brown - Treated & Graded Decking Joist (2\" x 2\")" });

            migrationBuilder.InsertData(
                table: "Joists",
                columns: new[] { "JoistId", "Price", "Type" },
                values: new object[] { 3, 2.6000000000000001, "Treated & Graded Decking Joist (3\" x 2\")" });

            migrationBuilder.InsertData(
                table: "Joists",
                columns: new[] { "JoistId", "Price", "Type" },
                values: new object[] { 4, 3.3199999999999998, "Treated & Graded Decking Joist (4\" x 2\")" });

            migrationBuilder.InsertData(
                table: "Lengths",
                columns: new[] { "LengthId", "Value" },
                values: new object[] { 15, 1.5 });

            migrationBuilder.InsertData(
                table: "Lengths",
                columns: new[] { "LengthId", "Value" },
                values: new object[] { 14, 1.8 });

            migrationBuilder.InsertData(
                table: "Lengths",
                columns: new[] { "LengthId", "Value" },
                values: new object[] { 13, 2.1000000000000001 });

            migrationBuilder.InsertData(
                table: "Lengths",
                columns: new[] { "LengthId", "Value" },
                values: new object[] { 12, 2.3999999999999999 });

            migrationBuilder.InsertData(
                table: "Lengths",
                columns: new[] { "LengthId", "Value" },
                values: new object[] { 11, 3.0 });

            migrationBuilder.InsertData(
                table: "Lengths",
                columns: new[] { "LengthId", "Value" },
                values: new object[] { 10, 3.0499999999999998 });

            migrationBuilder.InsertData(
                table: "Lengths",
                columns: new[] { "LengthId", "Value" },
                values: new object[] { 9, 3.2999999999999998 });

            migrationBuilder.InsertData(
                table: "Lengths",
                columns: new[] { "LengthId", "Value" },
                values: new object[] { 4, 4.5 });

            migrationBuilder.InsertData(
                table: "Lengths",
                columns: new[] { "LengthId", "Value" },
                values: new object[] { 7, 3.8999999999999999 });

            migrationBuilder.InsertData(
                table: "Lengths",
                columns: new[] { "LengthId", "Value" },
                values: new object[] { 6, 3.96 });

            migrationBuilder.InsertData(
                table: "Lengths",
                columns: new[] { "LengthId", "Value" },
                values: new object[] { 5, 4.2000000000000002 });

            migrationBuilder.InsertData(
                table: "Lengths",
                columns: new[] { "LengthId", "Value" },
                values: new object[] { 16, 1.2 });

            migrationBuilder.InsertData(
                table: "Lengths",
                columns: new[] { "LengthId", "Value" },
                values: new object[] { 3, 4.7999999999999998 });

            migrationBuilder.InsertData(
                table: "Lengths",
                columns: new[] { "LengthId", "Value" },
                values: new object[] { 2, 5.0999999999999996 });

            migrationBuilder.InsertData(
                table: "Lengths",
                columns: new[] { "LengthId", "Value" },
                values: new object[] { 1, 5.4000000000000004 });

            migrationBuilder.InsertData(
                table: "Lengths",
                columns: new[] { "LengthId", "Value" },
                values: new object[] { 8, 3.6000000000000001 });

            migrationBuilder.InsertData(
                table: "Lengths",
                columns: new[] { "LengthId", "Value" },
                values: new object[] { 17, 1.0 });

            migrationBuilder.InsertData(
                table: "BoardLength",
                columns: new[] { "BoardId", "LengthId" },
                values: new object[] { 1, 1 });

            migrationBuilder.InsertData(
                table: "BoardLength",
                columns: new[] { "BoardId", "LengthId" },
                values: new object[] { 2, 12 });

            migrationBuilder.InsertData(
                table: "BoardLength",
                columns: new[] { "BoardId", "LengthId" },
                values: new object[] { 4, 7 });

            migrationBuilder.InsertData(
                table: "BoardLength",
                columns: new[] { "BoardId", "LengthId" },
                values: new object[] { 4, 14 });

            migrationBuilder.InsertData(
                table: "BoardLength",
                columns: new[] { "BoardId", "LengthId" },
                values: new object[] { 1, 8 });

            migrationBuilder.InsertData(
                table: "BoardLength",
                columns: new[] { "BoardId", "LengthId" },
                values: new object[] { 2, 8 });

            migrationBuilder.InsertData(
                table: "BoardLength",
                columns: new[] { "BoardId", "LengthId" },
                values: new object[] { 3, 8 });

            migrationBuilder.InsertData(
                table: "BoardLength",
                columns: new[] { "BoardId", "LengthId" },
                values: new object[] { 4, 8 });

            migrationBuilder.InsertData(
                table: "BoardLength",
                columns: new[] { "BoardId", "LengthId" },
                values: new object[] { 2, 6 });

            migrationBuilder.InsertData(
                table: "BoardLength",
                columns: new[] { "BoardId", "LengthId" },
                values: new object[] { 5, 8 });

            migrationBuilder.InsertData(
                table: "BoardLength",
                columns: new[] { "BoardId", "LengthId" },
                values: new object[] { 2, 10 });

            migrationBuilder.InsertData(
                table: "BoardLength",
                columns: new[] { "BoardId", "LengthId" },
                values: new object[] { 1, 11 });

            migrationBuilder.InsertData(
                table: "BoardLength",
                columns: new[] { "BoardId", "LengthId" },
                values: new object[] { 2, 11 });

            migrationBuilder.InsertData(
                table: "BoardLength",
                columns: new[] { "BoardId", "LengthId" },
                values: new object[] { 4, 11 });

            migrationBuilder.InsertData(
                table: "BoardLength",
                columns: new[] { "BoardId", "LengthId" },
                values: new object[] { 5, 11 });

            migrationBuilder.InsertData(
                table: "BoardLength",
                columns: new[] { "BoardId", "LengthId" },
                values: new object[] { 5, 12 });

            migrationBuilder.InsertData(
                table: "BoardLength",
                columns: new[] { "BoardId", "LengthId" },
                values: new object[] { 1, 12 });

            migrationBuilder.InsertData(
                table: "BoardLength",
                columns: new[] { "BoardId", "LengthId" },
                values: new object[] { 2, 9 });

            migrationBuilder.InsertData(
                table: "BoardLength",
                columns: new[] { "BoardId", "LengthId" },
                values: new object[] { 5, 14 });

            migrationBuilder.InsertData(
                table: "BoardLength",
                columns: new[] { "BoardId", "LengthId" },
                values: new object[] { 1, 7 });

            migrationBuilder.InsertData(
                table: "BoardLength",
                columns: new[] { "BoardId", "LengthId" },
                values: new object[] { 5, 5 });

            migrationBuilder.InsertData(
                table: "BoardLength",
                columns: new[] { "BoardId", "LengthId" },
                values: new object[] { 4, 1 });

            migrationBuilder.InsertData(
                table: "BoardLength",
                columns: new[] { "BoardId", "LengthId" },
                values: new object[] { 5, 1 });

            migrationBuilder.InsertData(
                table: "BoardLength",
                columns: new[] { "BoardId", "LengthId" },
                values: new object[] { 1, 2 });

            migrationBuilder.InsertData(
                table: "BoardLength",
                columns: new[] { "BoardId", "LengthId" },
                values: new object[] { 4, 2 });

            migrationBuilder.InsertData(
                table: "BoardLength",
                columns: new[] { "BoardId", "LengthId" },
                values: new object[] { 1, 3 });

            migrationBuilder.InsertData(
                table: "BoardLength",
                columns: new[] { "BoardId", "LengthId" },
                values: new object[] { 2, 3 });

            migrationBuilder.InsertData(
                table: "BoardLength",
                columns: new[] { "BoardId", "LengthId" },
                values: new object[] { 4, 3 });

            migrationBuilder.InsertData(
                table: "BoardLength",
                columns: new[] { "BoardId", "LengthId" },
                values: new object[] { 5, 3 });

            migrationBuilder.InsertData(
                table: "BoardLength",
                columns: new[] { "BoardId", "LengthId" },
                values: new object[] { 4, 12 });

            migrationBuilder.InsertData(
                table: "BoardLength",
                columns: new[] { "BoardId", "LengthId" },
                values: new object[] { 1, 4 });

            migrationBuilder.InsertData(
                table: "BoardLength",
                columns: new[] { "BoardId", "LengthId" },
                values: new object[] { 2, 4 });

            migrationBuilder.InsertData(
                table: "BoardLength",
                columns: new[] { "BoardId", "LengthId" },
                values: new object[] { 1, 5 });

            migrationBuilder.InsertData(
                table: "BoardLength",
                columns: new[] { "BoardId", "LengthId" },
                values: new object[] { 2, 5 });

            migrationBuilder.InsertData(
                table: "BoardLength",
                columns: new[] { "BoardId", "LengthId" },
                values: new object[] { 4, 5 });

            migrationBuilder.InsertData(
                table: "BoardLength",
                columns: new[] { "BoardId", "LengthId" },
                values: new object[] { 4, 16 });

            migrationBuilder.InsertData(
                table: "JoistLength",
                columns: new[] { "JoistId", "LengthId" },
                values: new object[] { 4, 13 });

            migrationBuilder.InsertData(
                table: "JoistLength",
                columns: new[] { "JoistId", "LengthId" },
                values: new object[] { 1, 13 });

            migrationBuilder.InsertData(
                table: "JoistLength",
                columns: new[] { "JoistId", "LengthId" },
                values: new object[] { 1, 14 });

            migrationBuilder.InsertData(
                table: "JoistLength",
                columns: new[] { "JoistId", "LengthId" },
                values: new object[] { 3, 12 });

            migrationBuilder.InsertData(
                table: "JoistLength",
                columns: new[] { "JoistId", "LengthId" },
                values: new object[] { 1, 15 });

            migrationBuilder.InsertData(
                table: "JoistLength",
                columns: new[] { "JoistId", "LengthId" },
                values: new object[] { 4, 14 });

            migrationBuilder.InsertData(
                table: "JoistLength",
                columns: new[] { "JoistId", "LengthId" },
                values: new object[] { 3, 14 });

            migrationBuilder.InsertData(
                table: "JoistLength",
                columns: new[] { "JoistId", "LengthId" },
                values: new object[] { 2, 12 });

            migrationBuilder.InsertData(
                table: "JoistLength",
                columns: new[] { "JoistId", "LengthId" },
                values: new object[] { 1, 12 });

            migrationBuilder.InsertData(
                table: "JoistLength",
                columns: new[] { "JoistId", "LengthId" },
                values: new object[] { 4, 12 });

            migrationBuilder.InsertData(
                table: "JoistLength",
                columns: new[] { "JoistId", "LengthId" },
                values: new object[] { 1, 8 });

            migrationBuilder.InsertData(
                table: "JoistLength",
                columns: new[] { "JoistId", "LengthId" },
                values: new object[] { 3, 11 });

            migrationBuilder.InsertData(
                table: "JoistLength",
                columns: new[] { "JoistId", "LengthId" },
                values: new object[] { 1, 11 });

            migrationBuilder.InsertData(
                table: "JoistLength",
                columns: new[] { "JoistId", "LengthId" },
                values: new object[] { 4, 8 });

            migrationBuilder.InsertData(
                table: "JoistLength",
                columns: new[] { "JoistId", "LengthId" },
                values: new object[] { 3, 8 });

            migrationBuilder.InsertData(
                table: "JoistLength",
                columns: new[] { "JoistId", "LengthId" },
                values: new object[] { 2, 8 });

            migrationBuilder.InsertData(
                table: "JoistLength",
                columns: new[] { "JoistId", "LengthId" },
                values: new object[] { 4, 16 });

            migrationBuilder.InsertData(
                table: "JoistLength",
                columns: new[] { "JoistId", "LengthId" },
                values: new object[] { 1, 7 });

            migrationBuilder.InsertData(
                table: "JoistLength",
                columns: new[] { "JoistId", "LengthId" },
                values: new object[] { 4, 5 });

            migrationBuilder.InsertData(
                table: "JoistLength",
                columns: new[] { "JoistId", "LengthId" },
                values: new object[] { 1, 5 });

            migrationBuilder.InsertData(
                table: "JoistLength",
                columns: new[] { "JoistId", "LengthId" },
                values: new object[] { 4, 3 });

            migrationBuilder.InsertData(
                table: "JoistLength",
                columns: new[] { "JoistId", "LengthId" },
                values: new object[] { 3, 3 });

            migrationBuilder.InsertData(
                table: "JoistLength",
                columns: new[] { "JoistId", "LengthId" },
                values: new object[] { 2, 3 });

            migrationBuilder.InsertData(
                table: "JoistLength",
                columns: new[] { "JoistId", "LengthId" },
                values: new object[] { 1, 3 });

            migrationBuilder.InsertData(
                table: "JoistLength",
                columns: new[] { "JoistId", "LengthId" },
                values: new object[] { 4, 11 });

            migrationBuilder.InsertData(
                table: "JoistLength",
                columns: new[] { "JoistId", "LengthId" },
                values: new object[] { 4, 17 });

            migrationBuilder.CreateIndex(
                name: "IX_BoardLength_LengthId",
                table: "BoardLength",
                column: "LengthId");

            migrationBuilder.CreateIndex(
                name: "IX_JoistLength_LengthId",
                table: "JoistLength",
                column: "LengthId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "BoardLength");

            migrationBuilder.DropTable(
                name: "JoistLength");

            migrationBuilder.DropTable(
                name: "Boards");

            migrationBuilder.DropTable(
                name: "Joists");

            migrationBuilder.DropTable(
                name: "Lengths");
        }
    }
}
