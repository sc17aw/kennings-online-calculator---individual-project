// Filters page joists on page load, so only supported joists are shown

// On page load, the board lengths available will be checked for compatibility
// If there is no spacing which is compatible, an error is provided
// If there exists no board for a particular joist, that spacing is removed
// Any spacings which are compatible for at-least 1 spacing appear as normal

"use strict"

document.addEventListener("DOMContentLoaded", function() {
    var supportedSpacings = [600, 500, 450]; // Supported joists, in millimetres
    var joistOptions = document.getElementById("joist-input");

    for (var i = 0; i <= supportedSpacings.length - 1; ++i) {
        for (var j = 0; j <= boardObject.Lengths.length - 1; ++j) {
            if (isJoistCompatible(boardObject.Lengths[j], supportedSpacings[i])) {
                var option = document.createElement('option');
                option.value = supportedSpacings[i];
                option.innerText = supportedSpacings[i] + "mm";

                joistOptions.add(option);
                break; // Break, as only at-least 1 board is needed to be valid
            }
        }
    }

    if (joistOptions.children.length === 0) { // If no compatible spacings
        var errorDiv = displayInputError(
            "This board is not supported by the calculator."
        )
        var errorStyle = errorDiv.className;
        var returnDiv = document.getElementById("return");
        var inputDiv = document.getElementById('input-container');

        joistOptions.parentNode.parentNode.innerHTML = errorDiv.innerHTML;
        errorDiv.className = errorStyle;

        returnDiv.style = "margin: auto; margin-top: 2rem; display: block;";

        inputDiv.classList.add("calculator-errors");
        inputDiv.parentNode.appendChild(returnDiv);
    }
});