// Developed by Ashley Whiteley for Kennings Building Supplies
// Implements a heavily-modified Coin Change problem for decking boards

"use strict";

function calculateArea(width, length) {
    return parseFloat(width * length);
} // Returns area, given width and length


function countBoards(boards) {
    boards = boards.reverse();

    var counts = boards.reduce(function(count, board) {
        count[board] = (count[board] === undefined ? 1 : count[board] += 1);
        return count; // For each board, return the count
    }, {});

    return counts; // Return all of the counted types as a parsable object
} // Counts boards, returns a pseudo-dictionary for all board types


function calculateExtras(rows) {
    var area = calculatorState.area;

    // row[x].children[1] is the right column in that row.

    rows[1].children[1].innerHTML = Math.ceil(area / 5) + " boxes";
    rows[2].children[1].innerHTML = Math.ceil(area / 16) + " cans";
    rows[3].children[1].innerHTML = Math.ceil(area) + " m<sup>2</sup>";
} // Calculates the extras required for the calculations


function getJoists(width, joistSize) {
    width = width * 1000;
    var joistsRequired = parseInt(Math.ceil(width / joistSize)) + 1;

    return joistsRequired;
} // Calculate the joist requirements for a given configuration


function filterBoards(boards, joist) {
    return boards.filter(function(board) {
        return isJoistCompatible(board, joist)
    });
} // Filter all boards which cannot fit alongside the chosen joist within 10mm


function getOptimalConfig(boards, target) {
    if (parseFloat(target) < boards[0][4]) { // If target is less than lowest board, give lowest board automatically.
        return [boards[0][4]];
    }

    var results = [];

    while (target !== 0) { // If target has not been met
        var currBoard = null;

        for (var i = 0; i <= boards[0].length - 1; ++i) {
            // Go through all boards
            if (boards[0][i] === parseFloat(target)) {
                // If board fits the space required, add that board
                results.push(boards[0][i]);
                return results;
            }

            // If current board would underflow, keep track of that board
            if (boards[0][i] < parseFloat(target)) {
                currBoard = boards[0][i];
            }
        }

        // Update current target (so future boards will guarantee underflow)
        target = (target - currBoard).toFixed(2);
        results.push(currBoard); // Add the largest board that didn't overflow
    }
} // Performs a modified coin change problem to fill width/length


function getRemainder(boards) {
    var remainder = 0; // Holding variable of current padding.

    // 0.1 and 0.2 enables 1 decimal point coin change.
    // 0.01 and 0.02 enables 2 decimal point coin change.
    // To allow for 3 decimal + you'd need to add further padding:
    //  EG: 0.001 and 0.002 and 0.0001 and 0.0002 for 3 and 4 decimal points 

    boards.forEach(function(board) { // Sum up any coin change padding.
        if (board === 0.1 || board === 0.2 || board === 0.01 || board === 0.02) {
            remainder = remainder + board;
        }
    });

    return remainder;
} // Gets the remainder required to fill the user-defined width/length


function getConfig(boards, target, joist, existsSingle) {
    var results = getOptimalConfig(boards, target);
    var remainder = parseFloat(getRemainder(results).toFixed(2));
    var waste = remainder; // Remainder is now waste, as config is now overflowing.

    results = results.filter(function(board) {
        // Remove coin change padding (for 1 and 2 decimal coin change)
        return board !== 0.1 && board !== 0.2 && board !== 0.01 && board !== 0.02;
    });

    if (remainder > 0 && results.length > 0) { // If there is space left to fill
        if (results[results.length - 1] == boards[boards.length - 1]) {
            // If no large boards can be used, fill with small board (minimise overflow)
            results.push(boards[0][4]);
        }

        results = getOverflow(results, boards, remainder, target);
        waste = getWaste(results, target);

        if (waste > 0) {
            results = trimOverflow(results, boards, waste, joist);
        }
    }

    waste = getWaste(results, target);

    if (!existsSingle) { // If a configuration with a single board doesn't exist
        for (var i = 0; i <= boards[1].length - 1; ++i) {
            var singleWaste = parseFloat((boards[1][i] - target).toFixed(2));

            if (singleWaste >= 0 && singleWaste <= waste) { // If single board is better to use
                results = [boards[1][i]];
                waste = singleWaste;
                break;
            }
        }
    }

    return results;
} // Finds a board configuration with an optimal, trimmed overflow


function inputMenu() {
    document.getElementById("input-container").style.display = "block";
    document.getElementById("selection-container").style.display = "none";
    document.getElementById("summary-container").style.display = "none";
} // Reverts to the input menu by hiding other interfaces


function clearTable(table, size) {
    if (table.rows.length > size) { // Go through all rows
        while (table.rows.length > size) {
            table.deleteRow(size); // Delete rows
        }
    }
} // Clears previous board configurations from display table


function displayInputError(error) {
    var errorDisplay = document.getElementById("calculator-errors");
    errorDisplay.innerHTML = error;

    return errorDisplay;
} // Passes an error to the user interface


function isJoistCompatible(board, joist) {
    var JOIST_LENIENCY = 100; // 100mm joist leniency. (EG 5.1m is a valid board with 500mm joist)
    var difference = (board * 1000) % joist; // Find mm difference between joist spacing and board

    return difference <= JOIST_LENIENCY || difference - joist >= -JOIST_LENIENCY;
} // Function which determines if a joist is compatible with a board, given a joist leniency.


function calculateRows(length, width) {
    var DECKING_WIDTH = width; // Uses the board-associated width

    return parseInt(Math.ceil((length * 1000) / (DECKING_WIDTH + 5)));
} // Determines amount of rows for the length provided


function getTrimIndex(results, trim, boards) {
    if (trim.length > 0) { // If there was a trim attempted using trimOverflow()
        if (trim[0] !== -1) { // If a trim was made
            return results.indexOf(boards[1][trim[1]]); // Return trim index
        }
    }

    return results.length - 1; // If no trim was made, return the last board
} // Obtains the trim index for a given board configuration.


function selectionMenu() {
    document.getElementById("input-container").style.display = "none";
    document.getElementById("selection-container").style.display = "block";
    document.getElementById("display-container").style.display = "none";
} // Moves the calculator to the configuration selection menu.


function optionSelect(option) {
    var boardConfig = boardConfigs[option - 1]; // Option to array index.

    var view = document.getElementById('board-display');

    setSelectedView(
        view,
        boardConfig.results,
        boardConfig.proportions,
        boardConfig.trimIndex,
        calculatorState.joistsRequired
    );

    displayResults(
        boardConfig.results,
        boardConfig.waste,
        boardConfig.trimIndex,
        calculatorState.rows
    );

    displayMenu();
} // Facilitates the option select interface change, and displays the configuration.


function displayMenu() {
    document.getElementById("selection-container").style.display = "none";
    document.getElementById("summary-container").style.display = "none";
    document.getElementById("display-container").style.display = "block";
}


function displayJoistConfig(joistCount, joistTable, mp) {
    clearTable(joistTable, 2); // Clear any previous joist results

    for (var joist in joistCount) {
        var quantity = joistCount[joist];

        if (joistCount.hasOwnProperty(joist)) {
            var row = joistTable.insertRow(2);
            var joistType = row.insertCell(0);
            var joistQuantity = row.insertCell(1);

            joistType.innerHTML = '<td class="board-column">' + joist + "</td>";
            joistQuantity.innerHTML =
                '<td class="board-column">' + quantity * mp + "</td>";
        }
    }
} // Facilitates the display of a joist config for horizontal and vertical joists.


function setJoistWaste(waste, wasteTable) {
    if (waste === 0) {
        wasteTable.style.display = "none"; // Hide waste display if no waste

    } else {
        wasteTable.style.display = "";
        wasteTable.children[1].innerHTML = waste; // Show waste if there is waste
    }
} // Sets the joist waste for horizontal or vertical waste.


function getCombinedJoists(obj, counts, mp) {
    for (var key in counts) {
        if (obj[key] == null) { // If key doesn't current exist
            obj[key] = 0; // Create that key, with value of 0 (no boards)
        }

        obj[key] += counts[key] * mp; // Append or add to newly created key
    }

    return obj;
} // Takes current object (obj) and adds boards to new or existing dictionary keys


function displayJoists(joists, summaryTable, length, width) {
    var joistTables = document.getElementById("joist-tables");

    var horizontalWaste = getWaste(joistConfig.horizontal, width);
    var verticalWaste = getWaste(joistConfig.vertical, length);

    setJoistWaste(verticalWaste, joistTables.children[1]);
    setJoistWaste(horizontalWaste, joistTables.children[3]);

    var combined = new Object();
    combined = getCombinedJoists(combined, joistConfig.horizontalCount, 2);
    combined = getCombinedJoists(combined, joistConfig.verticalCount, joists);

    clearTable(summaryTable, 1); // Clear any previous joist results

    for (var joist in combined) {
        if (combined.hasOwnProperty(joist)) {
            var row = summaryTable.insertRow(1);
            var joistProduct = row.insertCell(0);
            var joistType = row.insertCell(1);
            var joistQuantity = row.insertCell(2);
            var joistPrice = row.insertCell(3);
            var joistTotal = row.insertCell(4); // Price of the joist per unit, using cost per metre

            var ppu = parseFloat(parseFloat(joist) * joistObject.Price);
            joistProduct.innerHTML = "<td>" + joistObject.Type + "</td>";
            joistType.innerHTML = "<td>" + joist + "m</td>";
            joistQuantity.innerHTML = "<td>" + combined[joist] + "</td>";
            joistPrice.innerHTML = "<td>£" + ppu.toFixed(2) + "</td>";
            joistTotal.innerHTML =
                "<td>£" + parseFloat(ppu * combined[joist]).toFixed(2) + "</td>";
        }
    }

    displayJoistConfig(
        joistConfig.verticalCount,
        joistTables.children[0],
        joists
    );

    displayJoistConfig(joistConfig.horizontalCount, joistTables.children[2], 2);
} // Displays the vertical and horizontal joists required. Combines them if they're identical



function displayResults(results, waste, trimIndex, rows) {
    var boardTable = document.getElementById("board-table");
    var summaryTable = document.getElementById("calculation-summary");
    var placeTable = document.getElementById("place-table");
    var cutTable = document.getElementById("cut-table");

    displayJoists(calculatorState.joistsRequired,
        summaryTable,
        calculatorState.length,
        calculatorState.width);

    if (waste !== 0) {
        var cutAmount = document.getElementById("cut-figure");
        var cutBoard = document.getElementById("board-figure");
        cutTable.style.display = "block";
        placeTable.style.display = "none";
        cutAmount.innerHTML = waste;
        cutBoard.innerHTML = results[trimIndex];

    } else {
        var endBoard = document.getElementById("place-figure");
        placeTable.style.display = "block";
        cutTable.style.display = "none";
        endBoard.innerHTML = results[trimIndex];
    }

    document.getElementById("rows-figure").innerHTML = rows;
    document.getElementById("waste-figure").innerHTML = waste; // Waste per row, not total

    results = deepCopy(results);
    var boardCount = countBoards(results);
    clearTable(boardTable, 1); // Clear any previous calculations

    for (var board in boardCount) {
        var value = boardCount[board];

        if (boardCount.hasOwnProperty(board)) {
            var detailRow = boardTable.insertRow(1);
            var detailType = detailRow.insertCell(0);
            var detailQuantity = detailRow.insertCell(1);

            detailType.innerHTML = '<td class="board-column">' + board + "</td>";
            detailQuantity.innerHTML =
                '<td class="board-column">' + value * rows + "</td>";

            var summaryRow = summaryTable.insertRow(1);
            var summaryProduct = summaryRow.insertCell(0);
            var summaryType = summaryRow.insertCell(1);
            var summaryQuantity = summaryRow.insertCell(2);
            var summaryPrice = summaryRow.insertCell(3);
            var summaryTotal = summaryRow.insertCell(4);

            // Price of the board per unit, using cost per metre
            var ppu = parseFloat(parseFloat(board) * boardObject.Price);

            summaryProduct.innerHTML = "<td>" + boardObject.Type + "</td>";
            summaryType.innerHTML = "<td>" + board + "m</td>";
            summaryQuantity.innerHTML = "<td>" + boardCount[board] * rows + "</td>";
            summaryPrice.innerHTML = "<td>£" + ppu.toFixed(2) + "</td>";
            summaryTotal.innerHTML = "<td>£" +
                parseFloat(ppu.toFixed(2) * boardCount[board] * rows).toFixed(2);
            "</td>";
        }
    }
} // Handles the display of a calculated board configuration in the interface


function isTrimmed(boards) {
    return Array.isArray(boards[0]);
} // Evaluation of boards to see if a trimming was performed


function ceilDecimal(decimal, digits) {
    var mp = Math.pow(10, digits); // Digit multiplier

    return parseFloat((Math.ceil((decimal * mp) * 100) / 100) / mp);
} // Custom ceiling rounding function to digits specified


function summaryMenu() {
    document.getElementById("display-container").style.display = "none";
    document.getElementById("summary-container").style.display = "block";
}


function createDisplayBoard(board, width) {
    var div = document.createElement("div");

    div.innerHTML = board;
    div.title = board;
    div.style.width = width + "%";

    return div;
} // Creates a specific board div, which can be used to generate a row of boards


function createDisplayRow(boards, proportions, trimIndex) {
    var row = [];

    var trimmed = [ // Get 2D array with trim board removed (red marked board)
        boards.splice(trimIndex, 1)[0],
        proportions.splice(trimIndex, 1)[0]
    ];

    for (var i = 0; i !== boards.length; ++i) { // Add boards to row
        var board = createDisplayBoard(boards[i], proportions[i]);
        row.push(board); // Add board to row.
    }

    var trimBoard = createDisplayBoard(trimmed[0], trimmed[1]);
    var trimContainer = document.createElement("div");
    var trimMarker = document.createElement("div");

    trimContainer.className = "trim-board";
    trimContainer.style.width = trimBoard.style.width;
    row.push(trimContainer); // Push trimmed board to current row

    trimBoard.style.width = "100%";
    trimBoard.className = "trimmed";
    trimContainer.appendChild(trimBoard); // Add trim

    trimMarker.className = "trim";
    trimContainer.appendChild(trimMarker);

    return row;
} // Creates a row using an array of proportions making up the row.


function getBestJoistConfig(lengths, target) {
    for (var i = 0; i <= 2; ++i) {
        var vertical = formatJoistConfig(getConfig(lengths, target, 0, 0));

        if (i == 0) {
            var currConfig = deepCopy(vertical);

        } else {
            if (getWaste(vertical, target) < currWaste) { // If new waste is less than previous
                currConfig = deepCopy(vertical); // Use new config
            }
        }

        if (lengths[0].length - 4 <= 2) { // If lengths - padding for coin change is 2 or less
            return currConfig;
        }

        var currWaste = getWaste(currConfig, target);
        removeBoard(vertical[vertical.length - 1], lengths); // Remove previously used joist
    }

    return currConfig; // Return the best config that was calculated
}

function getJoistConfig(width, length) {
    var joistLengths = deepCopy(joistObject.Lengths);

    var lengths = [
        joistLengths, // [1+ joists]
        joistLengths // [0-1 joists]
    ];

    lengths[0].unshift(0.01, 0.02, 0.1, 0.2); // Add padding used for coin change
    var vertical = getBestJoistConfig(deepCopy(lengths), length); // Find joist config

    if (width == length) {
        var horizontal = vertical; // If width == length, configs will be identical.

    } else {
        var horizontal = getBestJoistConfig(lengths, width);
    }

    return { // Return a joist config, both for vertical and horizontal joists
        vertical: vertical,
        horizontal: horizontal,
        verticalCount: countBoards(vertical),
        horizontalCount: countBoards(horizontal)
    };
} // Produces and returns a horizontal and vertical joist configuration


function formatJoistConfig(config) {
    if (isTrimmed(config)) {
        return config[0];
    }

    return config;
} // Removes any trimming information from joist config (not needed for config)


function getBoardConfig(boards, width, joist, existsSingle) {
    if (boards[1].length === 0) {
        // If no board lengths are available, no calculations are possible.
        return false;
    }

    var results = getConfig(boards, width, joist, existsSingle); // Get boards to fit

    if (isTrimmed(results)) { // Remove trimming information (no longer needed)
        var trim = results[1];
        results = results[0];

    } else {
        var trim = [];
    }

    if (results.length === 0) { // If no config can be made (no boards to use)
        return false;

    } else { // If config can be made, generate a config object
        return {
            boards: boards,
            results: results,
            trim: trim,
            proportions: getProportions(results, width),
            trimIndex: getTrimIndex(results, trim, boards),
            waste: getWaste(results, width)
        };
    }
} // Generates a configuration object based on the generated configuration


function generateBoardCollection(config) {
    var fragment = document.createDocumentFragment();

    config.forEach(function(board) {
        fragment.appendChild(board);
    });

    return fragment;
} // Creates a collection of divs, to reduce DOM refreshes


function reverseBoards(parent) {
    for (var row = 1; row < parent.childNodes.length; ++row) {
        parent.insertBefore(parent.childNodes[row], parent.firstChild);
    }
} // Reverse the inner div configuration for a given parent node


function clearView(view) {
    for (var row = 0; row !== view.children.length; ++row) {
        view.children[row].innerHTML = "";
    }
} // Clear the quick view board rows.


function setSelectedView(view, boards, proportions, trimIndex) {
    var boardsCopy = deepCopy(boards);
    var propCopy = deepCopy(proportions);
    var first = createDisplayRow(boardsCopy, propCopy, trimIndex); // First row

    // Cloning performed since DOM will not allow reuse of the same element
    first = generateBoardCollection(first);
    var second = first.cloneNode(true);
    var third = first.cloneNode(true);

    clearView(view);

    // Attach the board rows to the quick view
    view.children[0].appendChild(first);
    view.children[1].appendChild(second);
    view.children[2].appendChild(third);

    reverseBoards(view.children[1]); // Reverse second row (staggered effect)
    reverseBoards(view.children[1].children[0]); // Reverse trim board marking

} // Edits the quick view pane to display the configuration in the quick view


function getProportions(boards, target) {
    var proportions = [] // Array of proportions
    var boardsCopy = []

    boardsCopy = boards;

    boardsCopy.forEach(function(board) {
        proportions.push(ceilDecimal(board / target, 2) * 100);
    });

    return proportions;
} // Returns percentage proportion of board size in area


function sum(boards) {
    return parseFloat(boards.reduce(function(a, b) {
        return a + b;
    }, 0).toFixed(2));
} // Returns a board configuration's width/length coverage


function getWaste(boards, target) {
    if (isTrimmed(boards)) { // If the boards were subjected to trimOverflow()
        return parseFloat((sum(boards[0]) - target).toFixed(2));
    }

    return parseFloat((sum(boards) - target).toFixed(2));
} // Gets the waste for a given board configuration (board overflow)


function getOverflow(results, boards, remainder, target) {
    var tail = boards[0].indexOf(results[results.length - 1]);
    var waste = getWaste(results, target);

    for (var i = tail; i <= boards[0].length - 1; ++i) {
        if (boards[0][i] - (results[results.length - 1] + remainder) > 0) {
            results[results.length - 1] = boards[0][i];
            return results; // If the overflow made cannot be improved, return boards

        } else {
            if (parseFloat(waste) > 0) {
                return results; // If an overflow was created, return boards
            }
        }
    }

    results.push(boards[0][4]); // If no other board could fit, use the smallest board

    if (tail === boards[0].length - 1 && sum(results) > target) {
        return results;
    } // If only one board can be used, or overflow is present

    getOverflow(results, boards, remainder, target)
} // Gets the initial overflow that is compatible with joist choice


function trimOverflow(results, boards, waste, joist) {
    var trim = [-1, -1, waste] // [Changed index,  Board index, Trim]

    for (var i = results.length - 1; i >= results.length - 4; --i) {
        var tail = boards[1].indexOf(results[i]);

        for (var board = tail; board != 0 || trim[1] === 0; --board) {
            var diff = parseFloat(results[i] - boards[1][board]).toFixed(2);
            var currWaste = parseFloat((waste - diff).toFixed(2));

            if (diff <= waste) { // If there is a waste reduction
                if (currWaste < trim[2] && currWaste >= 0) {
                    trim = [i, board, waste]
                }
            }

            if (boards[1][tail] === undefined) { // If no trims can be made
                break;
            }
        } // Check all feasible boards
    }

    if (trim[0] >= 0) {
        results[trim[0]] = boards[1][trim[1]];

        if ((boards[1][trim[1]] * 1000 % joist) <= 50) {
            trim[0] = -1;
        }
    }

    var trimmed = [];
    trimmed.push(results, trim);
    return trimmed;
} // Returns an overflow, potentially with a single joist-incompatible board


function deepCopy(clone) {
    return clone.map(function(e) {
        return Array.isArray(e) ? deepCopy(e) : e;
    });
} // Obtains a deep copy of the array to clone. Currently only supports arrays


function getBoards(joist, availableLengths) {
    var boards = [
        [],
        availableLengths
    ];

    var filtered = filterBoards(boards[1], joist);

    filtered.unshift(0.01, 0.02, 0.1, 0.2);
    boards[0] = filtered;

    return boards;
} // Filters out incompatible boards


function validateNumber(value) {
    if (isNaN(value)) {
        return false;

    } else {
        return parseFloat((Math.ceil(parseFloat(value * 100).toFixed(2)) / 100)).toFixed(2);
    }
} // Checks to see if the number is a valid number (all chars numerical)


function getLength() {
    var length = document.getElementById("length-input").value;
    return validateNumber(length);
} // Obtains the length input from the DOM using 'length-input' ID


function getWidth() {
    var width = document.getElementById("width-input").value;
    return validateNumber(width);
} // Obtains the width input from the DOM using 'width-input' ID


function getJoist() {
    return parseInt(document.getElementById("joist-input").value);
} // Obtains the joist input from the DOM using 'joist-input' ID


function isInvalid(length, width) {
    var LENGTH_LIMIT = 100; // Limitations on board dimensions
    var WIDTH_LIMIT = 100;

    if (length === false || width === false) { // If input isn't a number
        return "Length and width must be numerical";
    }

    if (length.length === 0 || width.length === 0) { // If no input provided
        return "Length and width cannot be empty";
    }

    if (length > LENGTH_LIMIT || width > WIDTH_LIMIT) { // If exceeds limit
        return "Maximum " + LENGTH_LIMIT + "m lengths " +
            "and " + WIDTH_LIMIT + "m widths";
    }

    if (length <= 0 || width <= 0) { // If input is 0 or negative
        return "Length and width must be greater than 0";
    }

    return false;
} // Validators for the provided user input, returns false if valid


var calculatorState;

function inputSubmit() {
    var length = getLength(); // Obtain the user input
    var width = getWidth();

    var joist = getJoist();

    // Sort lengths in ascending order (this is required by the algorithm's approach)
    joistObject.Lengths = joistObject.Lengths.sort(function(a, b) { return a - b });
    boardObject.Lengths = boardObject.Lengths.sort(function(a, b) { return a - b });

    calculatorState = getCalculatorState(length, width, joist);

    var error;

    if (error = isInvalid(length, width)) {
        displayInputError(error);

    } else { // Input is valid, so continue with the calculation
        displayInputError("");

        calculateBoards(
            parseFloat(length).toFixed(2),
            parseFloat(width).toFixed(2),
            parseInt(joist),
            boardObject
        );
    }
} // Tied to the user input submit button


function getCalculatorState(length, width, joist) {
    return {
        joist: joist,
        length: parseFloat(length),
        width: parseFloat(width),
        rows: calculateRows(length, boardObject.Width),
        joistsRequired: getJoists(width, joist),
        area: calculateArea(width, length)
    }
} // Generates a singular state object, which simplifies information access


function removeBoard(board, boards) {
    var availableIndex = boards[0].indexOf(board);

    if (availableIndex >= 0) { // If board is in the joist-compatible board array
        boards[0].splice(availableIndex, 1);
    }

    boards[1].splice(boards[1].indexOf(board), 1);

    return boards;
} // Removes a specific board from the selection of available boards


var joistConfig; // Global variable, used to hold joist configurations.
var boardConfigs; // Global variable, used to hold board configurations.

function calculateBoards(length, width, joist, boardObject) {
    var t0 = performance.now(); // Get current time, used for ms performance checks.
    var optionalTable = document.getElementById("optional-summary").children[0];

    boardConfigs = [];

    var boards = getBoards(joist, boardObject.Lengths); // Get boards available.
    var boardCopy = deepCopy(boards); // Copy not by reference, but by values.

    calculateExtras(optionalTable.children); // Calculate extras like paint

    var existsSingle = false; // Flag, which determines if there is a single board config

    joistConfig = getJoistConfig(width, length);

    var selections = document.getElementById('selection-container').children[0].children;

    for (var i = 0; i <= 3; ++i) { // Forcefully generate up to 4 configs.
        var found = false;

        while (!found) { // Used to keep generating configs until a valid one is found
            var boardConfig = getBoardConfig(boardCopy, width, joist, existsSingle); // Generate config

            if (!boardConfig) { // If there is no config generated, no need to check - no more additions.
                break;
            }

            if (boardConfig.results.length === 1) {
                if (existsSingle) { // If there is a board config with a single board
                    removeBoard(boardConfig.results[0], boardCopy);

                } else {
                    existsSingle = true; // If config is single board, change flag so no more are generated.
                    found = true;
                }

            } else {
                found = true; // If the config is produced, and doesn't have a single board.
            }
        }

        // Uses the DOM tree to reduce queries. Selections is an already queried DOM object.
        var table = selections[i].children[1].children[0].children[1].children[0].children;
        var view = selections[i].children[2];

        if (!boardConfig) { // If config isn't generated successfully
            selections[i].style.display = "none";

        } else {
            selections[i].style.display = "block";

            setSelectedView(view, boardConfig.results, boardConfig.proportions, boardConfig.trimIndex);

            var boardCount = boardConfig.results.length * calculatorState.rows;
            var removal = boardConfig.results[boardConfig.results.length - 1]; // Get board last in board list

            table[0].innerHTML = boardCount;
            table[1].innerHTML = parseFloat(boardConfig.waste * calculatorState.rows).toFixed(2);

            boardCopy = removeBoard(removal, boardCopy); // Remove a board that was used previously.

            boardConfigs[i] = boardConfig; // Add the current config to the config list.
        }
    }

    selectionMenu();

    var t1 = performance.now();
    console.log("Calculation time: " + (t1 - t0) + " milliseconds.");

} // Handler for the board configuration calculation process