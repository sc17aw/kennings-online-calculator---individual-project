"use strict";

var joistObject = {
    "Id": 1,
    "Type": "Test Joist",
    "Price": 2.46,
    "Lengths": [
        1.5,
        1.8,
        2.1,
        2.4,
        3,
        3.6,
        3.9,
        4.2,
        4.8
    ]
}

var boardObject = {
    "Id": 1,
    "Type": "Test Board",
    "Width": 84,
    "Price": 1.17,
    "Lengths": [
        2.4,
        3,
        3.6,
        3.9,
        4.2,
        4.5,
        4.8,
        5.1,
        5.4
    ]
}

QUnit.test("Checking for successful calculator state #1 - getCalculatorState()", function(assert) {
    var length = 5;
    var width = 2;
    var joist = 500;

    var state = getCalculatorState(length, width, joist);

    assert.equal(state.length, 5, "State successfully generated");
});

QUnit.test("Checking for successful calculator state #2 - getCalculatorState()", function(assert) {
    var length = 10;
    var width = 3;
    var joist = 600;

    var state = getCalculatorState(length, width, joist);

    assert.equal(state.joist, 600, "State successfully generated");
});

QUnit.test("Row calculation #1 - calculateRows()", function(assert) {
    var length = 5;

    var rows = calculateRows(length, 84);

    assert.equal(rows, 57, "Correct rows calculated for 84mm width");
});

QUnit.test("Row calculation #2 - calculateRows()", function(assert) {
    var length = 5;

    var rows = calculateRows(length, 90);

    assert.equal(rows, 53, "Correct rows calculated for 90mm width");
});

QUnit.test("Row calculation #3 - calculateRows()", function(assert) {
    var length = 5;

    var rows = calculateRows(length, 145);

    assert.equal(rows, 34, "Correct rows calculated for 145mm width");
});

QUnit.test("Area calculation #1 - calculateArea()", function(assert) {
    var length = 5;
    var width = 6;

    var area = calculateArea(width, length);

    assert.equal(area, 30, "Correct area of 30m squared");
});

QUnit.test("Area calculation #2 - calculateArea()", function(assert) {
    var length = 25.5;
    var width = 3.4;

    var area = calculateArea(width, length);

    assert.equal(area, 86.7, "Correct area of 86.7m squared");
});

QUnit.test("Number validation checks #1 - validateNumber()", function(assert) {
    var value = 10

    var isValid = validateNumber(value);


    assert.equal(isValid, value, "Integer detected as valid");
});

QUnit.test("Number validation checks #2 - validateNumber()", function(assert) {
    var value = "notanumber"

    var isValid = validateNumber(value)

    assert.equal(isValid, false, "String detected as invalid");
});

QUnit.test("Number validation checks #3 - validateNumber()", function(assert) {
    var value = 15.4

    var isValid = validateNumber(value)

    assert.equal(isValid, value, "Float detected as valid");
});


QUnit.test("Getting initial overflow #1 - getOverflow()", function(assert) {
    var boards = [
        [0.01, 0.02, 0.1, 0.2, 1.5, 1.8, 2.1, 2.4, 3, 4.8],
        [0.01, 0.02, 0.1, 0.2, 1.5, 1.8, 2.1, 2.4, 3, 4.8]
    ]

    var results = [4.8, 4.8, 3.9]
    var remainder = 0.6
    var target = 13.20

    var overflow = JSON.stringify(getOverflow(results, boards, remainder, target))

    assert.equal(overflow, JSON.stringify([4.8, 4.8, 3.9]), "Initial overflow found");
});

QUnit.test("Getting initial overflow #2 - getOverflow()", function(assert) {
    var boards = [
        [0.01, 0.02, 0.1, 0.2, 1.5, 1.8, 2.1, 2.4, 3, 3.6, 4.2, 4.8],
        [0.01, 0.02, 0.1, 0.2, 1.5, 1.8, 2.1, 2.4, 3, 3.6, 4.2, 4.8]
    ]

    var results = [3.9]
    var remainder = 0.1
    var target = 4.00

    var overflow = JSON.stringify(getOverflow(results, boards, remainder, target))

    assert.equal(overflow, JSON.stringify([4.2]), "Initial overflow found");
});

QUnit.test("Getting initial overflow #3 - getOverflow()", function(assert) {
    var boards = [
        [0.01, 0.02, 0.1, 0.2, 1.8, 2.1, 2.4, 3, 4.2],
        [0.01, 0.02, 0.1, 0.2, 1.8, 2.1, 2.4, 3, 4.2]
    ]

    var results = [4.2, 4.2, 4.2, 4.2, 2.4];
    var remainder = 0.1;
    var target = 19.30;

    var overflow = JSON.stringify(getOverflow(results, boards, remainder, target));

    assert.equal(overflow, JSON.stringify([4.2, 4.2, 4.2, 4.2, 3]), "Initial overflow found");
});


QUnit.test("Overflow trimming checks #1 - trimOverflow()", function(assert) {
    var boards = [
        [0.01, 0.02, 0.1, 0.2, 1.5, 1.8, 2.1, 2.4, 3, 3.6, 4.2, 4.8],
        [1.5, 1.8, 2.1, 2.4, 3, 3.6, 4.2, 4.8]
    ];

    var results = [5.4, 4.2];
    var waste = 0.2;
    var joist = 600;

    var trim = JSON.stringify(trimOverflow(results, boards, waste, joist)[0]);

    assert.equal(trim, JSON.stringify([5.4, 4.2]), "Overflow trim not performed where not needed");
});


QUnit.test("Overflow trimming checks #2 - trimOverflow()", function(assert) {
    var boards = [
        [0.01, 0.02, 0.1, 0.2, 2.4, 3, 3.6, 5.4],
        [2.4, 3, 3.6, 3.9, 4.5, 5.1, 5.4]
    ];

    var results = [5.4, 5.4];
    var waste = 1.4;
    var joist = 600;

    var trim = JSON.stringify(trimOverflow(results, boards, waste, joist)[0])

    assert.equal(trim, JSON.stringify([4.5, 5.4]), "Overflow trim for 600mm joist performed");
});

QUnit.test("Overflow trimming checks #3 - trimOverflow()", function(assert) {
    var boards = [
        [0.01, 0.02, 0.1, 0.2, 2.4, 3, 3.6, 3.9, 5.1, 5.4],
        [2.4, 3, 3.6, 3.9, 4.2, 4.8, 5.1, 5.4]
    ];

    var results = [5.4, 5.1];
    var waste = 1.1;
    var joist = 500;

    var trim = JSON.stringify(trimOverflow(results, boards, waste, joist)[0])

    assert.equal(trim, JSON.stringify([4.8, 5.1]), "Overflow trim for 500mm joist performed");
});

QUnit.test("Obtaining waste for configuration #1 - getWaste()", function(assert) {
    var boards = [3.6, 2.1];
    var target = 5.30;

    var waste = getWaste(boards, target)

    assert.equal(waste, 0.4, "Found waste where there was positive waste (overflow)");
});


QUnit.test("Obtaining waste for configuration #2 - getWaste()", function(assert) {
    var boards = [4.8];
    var target = 5.30;

    var waste = getWaste(boards, target)

    assert.equal(waste, -0.5, "Found waste where there was negative waste (underflow)");
});

QUnit.test("Obtaining proportions #1 - getProportions()", function(assert) {
    var boards = [3.9];
    var target = 3.9;

    var prop = JSON.stringify(getProportions(boards, target))

    assert.equal(prop, JSON.stringify([100]), "100% proportion calculated");
});

QUnit.test("Obtaining proportions #2 - getProportions()", function(assert) {
    var boards = [5.4, 5.4, 2.4];
    var target = 13.20;

    var prop = JSON.stringify(getProportions(boards, target))

    assert.equal(prop, JSON.stringify([40.91, 40.91, 18.19]), "Multiple proportions calculated");
});

QUnit.test("Obtaining joist configurations #1 - getBestJoistConfig()", function(assert) {
    var boards = [
        [0.01, 0.02, 0.1, 0.2, 1.5, 1.8, 2.1, 2.4, 3, 3.6, 3.9, 4.2, 4.8],
        [0.01, 0.02, 0.1, 0.2, 1.5, 1.8, 2.1, 2.4, 3, 3.6, 3.9, 4.2, 4.8]
    ];

    var length = 5.90;

    var config = JSON.stringify(getBestJoistConfig(boards, length))

    assert.equal(config, JSON.stringify([4.2, 1.8]), "Joist configuration calculated");
});

QUnit.test("Obtaining joist configurations #2 - getBestJoistConfig()", function(assert) {
    var boards = [
        [0.01, 0.02, 0.1, 0.2, 1.8, 2.4, 3, 3.6, 4.8],
        [0.01, 0.02, 0.1, 0.2, 1.8, 2.4, 3, 3.6, 4.8]
    ];

    var length = 19.40;

    var config = JSON.stringify(getBestJoistConfig(boards, length))

    assert.equal(config, JSON.stringify([4.8, 3.6, 4.8, 4.8, 1.8]), "Joist configuration calculated");
});

QUnit.test("Obtaining joist configurations #3 - getBestJoistConfig()", function(assert) {
    var boards = [
        [0.01, 0.02, 0.1, 0.2, 1, 1.2, 1.8, 2.1, 2.4, 3, 3.6, 4.2, 4.8],
        [0.01, 0.02, 0.1, 0.2, 1, 1.2, 1.8, 2.1, 2.4, 3, 3.6, 4.2, 4.8]
    ];

    var length = 9.35;

    var config = JSON.stringify(getBestJoistConfig(boards, length))

    assert.equal(config, JSON.stringify([4.2, 4.2, 1]), "Joist configuration calculated");
});

QUnit.test("Obtaining combined joists #1 - getCombinedJoists()", function(assert) {
    var horizontal = { "2.4": 1, "4.8": 2, "3.6": 1 };
    var vertical = { "2.4": 1, "4.8": 1, "3.6": 1 };
    var joists = 27;

    var combined = new Object();
    combined = getCombinedJoists(combined, horizontal, 2);
    combined = getCombinedJoists(combined, vertical, joists);

    assert.equal(combined["4.8"], 31, "Joist configurations combined");
});

QUnit.test("Obtaining combined joists #2 - getCombinedJoists()", function(assert) {
    var horizontal = { "2.4": 1, "3.6": 5 };
    var vertical = { "2.4": 1, "4.8": 2, "3.6": 1 };
    var joists = 41;

    var combined = new Object();
    combined = getCombinedJoists(combined, horizontal, 2);
    combined = getCombinedJoists(combined, vertical, joists);

    assert.equal(combined["4.8"], 82, "Joist configurations combined");
});

QUnit.test("Filtering boards with joists #1 - filterBoards()", function(assert) {
    var boards = [2.4, 3, 3.05, 3.3, 3.6, 3.96, 4.2, 4.5, 4.8];

    var filtered = JSON.stringify(filterBoards(boards, 600));

    assert.equal(filtered, JSON.stringify([2.4, 3, 3.05, 3.6, 4.2, 4.8]), "Boards filtered");
});

QUnit.test("Filtering boards with joists #2 - filterBoards()", function(assert) {
    var boards = [1.2, 1.8, 2.4, 3, 3.6, 3.9, 4.2, 4.8, 5.1, 5.4]

    var filtered = JSON.stringify(filterBoards(boards, 500))

    assert.equal(filtered, JSON.stringify([2.4, 3, 3.6, 3.9, 5.1, 5.4]), "Boards filtered");
});

QUnit.test("Generation of board config #1 - getConfig()", function(assert) {
    var boards = [
        [0.01, 0.02, 0.1, 0.2, 2.4, 3, 3.6, 4.2, 4.8, 5.4],
        [0.01, 0.02, 0.1, 0.2, 2.4, 3, 3.6, 3.9, 4.2, 4.5, 4.8, 5.1, 5.4]
    ];

    var width = 13.20;
    var joist = 600;
    var existsSingle = false;

    var config = JSON.stringify(getConfig(boards, width, joist, existsSingle))

    assert.equal(config, JSON.stringify([5.4, 5.4, 2.4]), "Board config generated");
});

QUnit.test("Generation of board config #2 - getConfig()", function(assert) {
    var boards = [
        [0.01, 0.02, 0.1, 0.2, 2.4, 3, 3.05, 3.6, 4.2, 4.8],
        [0.01, 0.02, 0.1, 0.2, 3, 3.05, 3.3, 3.6, 3.96, 4.2, 4.5, 4.8]
    ];

    var width = 15.20;
    var joist = 600;
    var existsSingle = false;

    var config = JSON.stringify(getConfig(boards, width, joist, existsSingle)[0])

    assert.equal(config, JSON.stringify([3.3, 4.8, 4.8, 2.4]), "Board config generated");
});

QUnit.test("Generation of board config #3 - getConfig()", function(assert) {
    var boards = [
        [0.01, 0.02, 0.1, 0.2, 3.6],
        [3.6]
    ];

    var width = 3.40;
    var joist = 500;
    var existsSingle = false;

    var config = JSON.stringify(getConfig(boards, width, joist, existsSingle))

    assert.equal(config, JSON.stringify([3.6]), "Board config generated");
});

QUnit.test("Calculate joist requirements #1 - getJoists()", function(assert) {
    var width = 10.2;
    var joistSize = 600;

    var required = getJoists(width, joistSize);

    assert.equal(required, 18, "Obtained required joists");
});

QUnit.test("Calculate joist requirements #2 - getJoists()", function(assert) {
    var width = 20.5;
    var joistSize = 500;

    var required = getJoists(width, joistSize);

    assert.equal(required, 42, "Obtained required joists");
});

QUnit.test("Count boards in configuration #1 - countBoards()", function(assert) {
    var boards = [4.8, 4.8, 4.8, 3.2, 1.8]

    var count = countBoards(boards);

    assert.equal(count["4.8"], 3, "Obtained board count");
});

QUnit.test("Count boards in configuration #2 - countBoards()", function(assert) {
    var boards = [3.2, 3.2, 2.8, 1.8, 1.5, 1.5, 1.5]

    var count = countBoards(boards);

    assert.equal(count["1.8"], 1, "Obtained board count");
});

QUnit.test("Count boards in configuration #3 - countBoards()", function(assert) {
    var boards = [5.4, 5.4, 5.4, 5.4, 5.4, 4.8, 4.8, 3.3]

    var count = countBoards(boards);

    assert.equal(count["5.4"], 5, "Obtained board count");
});

QUnit.test("Remove board in configuration #1 - countBoards()", function(assert) {
    var boards = [
        [0.01, 0.02, 0.1, 0.2, 1.5, 1.8, 2.1, 2.4, 3, 3.6, 3.9, 4.2, 4.8],
        [0.01, 0.02, 0.1, 0.2, 1.5, 1.8, 2.1, 2.4, 3, 3.6, 3.9, 4.2, 4.8]
    ];

    var boards = removeBoard(3.6, boards);
    var found = boards[0].includes(3.6);

    assert.equal(found, false, "Removed board from selection");
});

QUnit.test("Remove board in configuration #2 - countBoards()", function(assert) {
    var boards = [
        [0.01, 0.02, 0.1, 0.2, 1.5, 1.8, 2.4, 4.2, 4.8],
        [0.01, 0.02, 0.1, 0.2, 1.5, 1.8, 2.4, 4.2, 4.8]
    ];

    var boards = removeBoard(4.8, boards);
    var found = boards[0].includes(3.6);

    assert.equal(found, false, "Removed board from selection");
});