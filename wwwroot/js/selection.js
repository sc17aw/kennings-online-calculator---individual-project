"use strict"

function checkInput() {
    var submit = document.getElementById('calc-button');
    var joist = document.getElementById('joist-input').value;
    var board = document.getElementById('board-input').value;

    joist > 0 && board > 0 ? submit.disabled = false : submit.disabled = true;

}