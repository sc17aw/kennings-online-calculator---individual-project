using System;
using System.Dynamic;
using System.Collections.Generic;
using System.Text.Json;

using kennings_aspdotnet.Models;

public class BoardLengthModel
{
  public Board Board { get; set; }
  public Joist Joist { get; set; }

  public String GetBoardInformation() 
  {
    var BoardLengths = new List<double>();
    dynamic obj = new ExpandoObject();
   
    foreach (var board in Board.BoardLengths) {
      BoardLengths.Add(board.Length.Value);
    }

    obj.Id = Board.BoardId;
    obj.Type = Board.Type;
    obj.Width = Board.Width;
    obj.Price = Board.Price;
    obj.Lengths = BoardLengths;

    return JsonSerializer.Serialize(obj);
  }

  public String GetJoistInformation() 
  {
    var JoistLengths = new List<double>();
    dynamic obj = new ExpandoObject();
   
    foreach (var joist in Joist.JoistLengths) {
      JoistLengths.Add(joist.Length.Value);
    }

    obj.Id = Joist.JoistId;
    obj.Type = Joist.Type;
    obj.Price = Joist.Price;
    obj.Lengths = JoistLengths;

    return JsonSerializer.Serialize(obj);
  }
}