using System.ComponentModel.DataAnnotations;

namespace kennings_aspdotnet.Models
{
    public class JoistLength
    {
      [Required]
      public int JoistId { get; set; }
      public Joist Joist { get; set; }

      [Required]
      public int LengthId { get; set; }
      public Length Length { get; set; } 
    }
}
