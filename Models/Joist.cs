using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace kennings_aspdotnet.Models
{
    public class Joist
    {
        [Required]
        public int JoistId { get; set; } // DB ID
        
        [Required]
        [MaxLength(100)]
        public string Type { get; set; } // Joist name, type.
        
        [Required]
        [Column(TypeName="Money")]
        public double Price { get; set; } // Price per m

        public virtual ICollection<JoistLength> JoistLengths { get; set; }
    }
}
