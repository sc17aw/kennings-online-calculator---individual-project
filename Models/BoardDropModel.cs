using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.Rendering;
using kennings_aspdotnet.Models;

public class BoardDropView
{
  public Board[] Boards { get; set; }
  public Joist[] Joists { get; set; }

  public IEnumerable<SelectListItem> GetBoardSelectList() 
  {
    var boardSelectList = new List<SelectListItem>();

    foreach (var board in Boards) {
      boardSelectList.Add(new SelectListItem {
        Value = board.BoardId.ToString(),
        Text = board.Type
      });
    }

    return boardSelectList;
  }

  public IEnumerable<SelectListItem> GetJoistSelectList() 
  {
    var joistSelectList = new List<SelectListItem>();

    foreach (var joist in Joists) {
      joistSelectList.Add(new SelectListItem {
        Value = joist.JoistId.ToString(),
        Text = joist.Type
      });
    }

    return joistSelectList;
  }
}