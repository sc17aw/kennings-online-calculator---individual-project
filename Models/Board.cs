using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace kennings_aspdotnet.Models
{
    public class Board
    {
        [Required]
        public int BoardId { get; set; } // DB ID

        [Required]
        [MaxLength(100)]
        public string Type { get; set; } // Board name, type.

        [Required]
        public int Width { get; set; } // Width of the board (90mm, 120mm etc)

        [Required]
        [Column(TypeName="Money")]
        public double Price { get; set; } // Price per m

        public virtual ICollection<BoardLength> BoardLengths { get; set; } // Boards used by this length
    }
}
