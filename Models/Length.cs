using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace kennings_aspdotnet.Models
{
    public class Length
    {
        [Required]
        public int LengthId { get; set; } // DB ID

        [Required]
        public double Value { get; set; } // Length value, EG: 5.1, 3, 1.8 etc..
        public virtual ICollection<BoardLength> BoardLengths { get; set; }
        public virtual ICollection<JoistLength> JoistLengths { get; set; }
    }
}
