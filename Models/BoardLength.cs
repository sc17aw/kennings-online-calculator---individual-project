using System.ComponentModel.DataAnnotations;

namespace kennings_aspdotnet.Models
{
    public class BoardLength
    {
      [Required]
      public int BoardId { get; set; }

      public Board Board { get; set; }

      [Required]
      public int LengthId { get; set; }

      public Length Length { get; set; } 
    }
}
