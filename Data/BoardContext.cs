using Microsoft.EntityFrameworkCore;
using kennings_aspdotnet.Models;

namespace kennings_aspdotnet.Data
{
    public class BoardContext : DbContext
    {
        public BoardContext (DbContextOptions<BoardContext> options) : base(options)
        {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder) {
          optionsBuilder.UseSqlite("Data Source=kennings.db");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder) {
          base.OnModelCreating(modelBuilder);
          modelBuilder.Entity<BoardLength>().HasKey(key => new { key.BoardId, key.LengthId });
          modelBuilder.Entity<JoistLength>().HasKey(key => new { key.JoistId, key.LengthId });
          
          modelBuilder.Entity<Board>().HasData(
            new Board 
            {
              BoardId = 1,
              Type = "Discount Decking (84mm x 19mm)",
              Width = 84,
              Price = 1.17
            }, // https://www.edecks.co.uk/products/1964/215/17/0/Discount-Decking-(84mm-x-19mm)

            new Board
            {
              BoardId = 2,
              Type = "Smooth Hardwood Balau Decking (90mm x 19mm)",
              Width = 90,
              Price = 4.87
            }, // https://www.edecks.co.uk/products/158/216/18/0/Smooth-Hardwood-Balau-Decking-(90mm-x-19mm)

            new Board
            {
              BoardId = 3,
              Type = "Dark Brown Larch Brushwood Finish Decking Board (145mm x 38mm)",
              Width = 145,
              Price = 4.89
            }, // https://www.edecks.co.uk/products/28473/216/18/0/Dark-Brown-Larch-Brushwood-Finish-Decking-Board-(145mm-x-38mm)

            new Board
            {
              BoardId = 4,
              Type = "Standard Redwood Decking (120mm x 28mm)",
              Width = 120,
              Price = 1.74
            },  // https://www.edecks.co.uk/products/170/215/17/0/Standard-Redwood-Decking-(120mm-x-28mm)

            new Board
            {
              BoardId = 5,
              Type = "Brown - Standard Redwood Decking (120mm x 28mm)",
              Width = 120,
              Price = 3.51
            }  // https://www.edecks.co.uk/products/21790/215/17/0/Brown---Standard-Redwood-Decking-(120mm-x-28mm)
          );

          modelBuilder.Entity<Joist>().HasData(
            new Joist 
            {
              JoistId = 1,
              Type = @"Treated & Graded Decking Joist (3 1/2"" x 1 1/2"")",
              Price = 2.46
            }, // https://www.edecks.co.uk/products/77/224/103/0/Treated-Graded-Decking-Joist-(3-1/2-x-1-1/2)

            new Joist
            {
              JoistId = 2,
              Type = @"Brown - Treated & Graded Decking Joist (2"" x 2"")",
              Price = 2.15
            }, // https://www.edecks.co.uk/products/32058/224/103/0/Brown---Treated-Graded-Decking-Joist-(2-x-2)

            new Joist
            {
              JoistId = 3,
              Type = @"Treated & Graded Decking Joist (3"" x 2"")",
              Price = 2.60
            }, // https://www.edecks.co.uk/products/540/224/103/0/Treated-Graded-Decking-Joist-(3-x-2)

            new Joist
            {
              JoistId = 4,
              Type = @"Treated & Graded Decking Joist (4"" x 2"")",
              Price = 3.32
            }  // https://www.edecks.co.uk/products/110/224/103/0/Treated-Graded-Decking-Joist-(4-x-2)
          );

          modelBuilder.Entity<Length>().HasData(
            new Length 
            {
              LengthId = 1,
              Value = 5.4
            },

            new Length
            {
              LengthId = 2,
              Value = 5.1
            },

            new Length
            {
              LengthId = 3,
              Value = 4.8
            },

            new Length
            {
              LengthId = 4,
              Value = 4.5
            },

            new Length
            {
              LengthId = 5,
              Value = 4.2
            },

            new Length
            {
              LengthId = 6,
              Value = 3.96
            },

            new Length
            {
              LengthId = 7,
              Value = 3.9
            },

            new Length
            {
              LengthId = 8,
              Value = 3.6
            },

            new Length
            {
              LengthId = 9,
              Value = 3.3
            },

            new Length
            {
              LengthId = 10,
              Value = 3.05
            },

            new Length
            {
              LengthId = 11,
              Value = 3.0
            },

            new Length
            {
              LengthId = 12,
              Value = 2.4
            },

            new Length
            {
              LengthId = 13,
              Value = 2.1
            },

            new Length
            {
              LengthId = 14,
              Value = 1.8
            },

            new Length
            {
              LengthId = 15,
              Value = 1.5
            },

            new Length
            {
              LengthId = 16,
              Value = 1.2
            },

            new Length
            {
              LengthId = 17,
              Value = 1.0
            }
          );

          modelBuilder.Entity<BoardLength>().HasData(
            // First - Discount-Decking-(84mm-x-19mm)
            new BoardLength { BoardId = 1, LengthId = 1 }, // 5.4m length
            new BoardLength { BoardId = 1, LengthId = 2 }, // 5.1m length
            new BoardLength { BoardId = 1, LengthId = 3 }, // 4.8m length
            new BoardLength { BoardId = 1, LengthId = 4 }, // 4.5m length
            new BoardLength { BoardId = 1, LengthId = 5 }, // 4.2m length
            new BoardLength { BoardId = 1, LengthId = 7 }, // 3.9m length
            new BoardLength { BoardId = 1, LengthId = 8 }, // 3.6m length
            new BoardLength { BoardId = 1, LengthId = 11 }, // 3.0m length
            new BoardLength { BoardId = 1, LengthId = 12 }, // 2.4m length

            // Second - Smooth-Hardwood-Balau-Decking-(90mm-x-19mm)
            new BoardLength { BoardId = 2, LengthId = 3 }, // 4.8m length
            new BoardLength { BoardId = 2, LengthId = 4 }, // 4.5m length
            new BoardLength { BoardId = 2, LengthId = 5 }, // 4.2m length
            new BoardLength { BoardId = 2, LengthId = 6 }, // 3.96m length
            new BoardLength { BoardId = 2, LengthId = 8 }, // 3.6m length
            new BoardLength { BoardId = 2, LengthId = 9 }, // 3.3m length
            new BoardLength { BoardId = 2, LengthId = 10 }, // 3.05m length
            new BoardLength { BoardId = 2, LengthId = 11 }, // 3m length
            new BoardLength { BoardId = 2, LengthId = 12 }, // 2.4m length

            // Third - Dark-Brown-Larch-Brushwood-Finish-Decking-Joist-(145mm-x-38mm)
            new BoardLength { BoardId = 3, LengthId = 8 }, // 3.6m length

            // Fourth - Standard Redwood Decking (120mm x 28mm)
            new BoardLength { BoardId = 4, LengthId = 1 }, // 5.4m length
            new BoardLength { BoardId = 4, LengthId = 2 }, // 5.1m length
            new BoardLength { BoardId = 4, LengthId = 3 }, // 4.8m length
            new BoardLength { BoardId = 4, LengthId = 5 }, // 4.2m length
            new BoardLength { BoardId = 4, LengthId = 7 }, // 3.9m length
            new BoardLength { BoardId = 4, LengthId = 8 }, // 3.6m length
            new BoardLength { BoardId = 4, LengthId = 11 }, // 3.0m length
            new BoardLength { BoardId = 4, LengthId = 12 }, // 2.4m length
            new BoardLength { BoardId = 4, LengthId = 14 }, // 1.8m length
            new BoardLength { BoardId = 4, LengthId = 16 }, // 1.2m length

            // Fifth - Brown - Standard Redwood Decking (120mm x 28mm)
            new BoardLength { BoardId = 5, LengthId = 1 }, // 5.4m length
            new BoardLength { BoardId = 5, LengthId = 3 }, // 4.8m length
            new BoardLength { BoardId = 5, LengthId = 5 }, // 4.2m length
            new BoardLength { BoardId = 5, LengthId = 8 }, // 3.6m length
            new BoardLength { BoardId = 5, LengthId = 11 }, // 3.0m length
            new BoardLength { BoardId = 5, LengthId = 12 }, // 2.4m length
            new BoardLength { BoardId = 5, LengthId = 14 } // 1.8m length
          );

          modelBuilder.Entity<JoistLength>().HasData(
            // First - Treated-Graded-Decking-Joist-(3-1/2-x-1-1/2)
            new JoistLength { JoistId = 1, LengthId = 3 }, // 4.8m length
            new JoistLength { JoistId = 1, LengthId = 5 }, // 4.2m length
            new JoistLength { JoistId = 1, LengthId = 7 }, // 3.9m length
            new JoistLength { JoistId = 1, LengthId = 8 }, // 3.6m length
            new JoistLength { JoistId = 1, LengthId = 11 }, // 3.0m length
            new JoistLength { JoistId = 1, LengthId = 12 }, // 2.4m length
            new JoistLength { JoistId = 1, LengthId = 13 }, // 2.1m length
            new JoistLength { JoistId = 1, LengthId = 14 }, // 1.8m length
            new JoistLength { JoistId = 1, LengthId = 15 }, // 1.5m length

            // Second - Brown---Treated-Graded-Decking-Joist-(2-x-2)
            new JoistLength { JoistId = 2, LengthId = 3 }, // 4.8m length
            new JoistLength { JoistId = 2, LengthId = 8 }, // 3.6m length
            new JoistLength { JoistId = 2, LengthId = 12 }, // 2.4m length

            // Third - Treated-Graded-Decking-Joist-(3-x-2)
            new JoistLength { JoistId = 3, LengthId = 3 }, // 4.8m length
            new JoistLength { JoistId = 3, LengthId = 8 }, // 3.6m length
            new JoistLength { JoistId = 3, LengthId = 11 }, // 3.0m length
            new JoistLength { JoistId = 3, LengthId = 12 }, // 2.4m length
            new JoistLength { JoistId = 3, LengthId = 14 }, // 1.8m length

            // Fourth - Treated-Graded-Decking-Joist-(4-x-2)
            new JoistLength { JoistId = 4, LengthId = 3 }, // 4.8m length
            new JoistLength { JoistId = 4, LengthId = 5 }, // 4.2m length
            new JoistLength { JoistId = 4, LengthId = 8 }, // 3.6m length
            new JoistLength { JoistId = 4, LengthId = 11 }, // 3.0m length
            new JoistLength { JoistId = 4, LengthId = 12 }, // 2.4m length
            new JoistLength { JoistId = 4, LengthId = 13 }, // 2.1m length
            new JoistLength { JoistId = 4, LengthId = 14 }, // 1.8m length
            new JoistLength { JoistId = 4, LengthId = 16 }, // 1.2m length
            new JoistLength { JoistId = 4, LengthId = 17 }  // 1m length
          );
        }

        public DbSet<Board> Boards { get; set; }
        public DbSet<Joist> Joists { get; set; }
        public DbSet<Length> Lengths { get; set; }

        public DbSet<BoardLength> JoistLengths { get; set; }
        public DbSet<BoardLength> BoardLengths { get; set; }
    }
}