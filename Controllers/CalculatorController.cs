﻿using System.Diagnostics;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using kennings_aspdotnet.Data;
using kennings_aspdotnet.Models;

using System;

namespace kennings_aspdotnet.Controllers
{
    public class CalculatorController : Controller
    {
        private BoardContext _db;

        public CalculatorController(BoardContext context) {
            _db = context;
        }

        [HttpGet]
        public async Task<IActionResult> Selection() {
            using (_db) {
                var model = new BoardDropView {
                    Boards = await _db.Boards.AsNoTracking().ToArrayAsync(),
                    Joists = await _db.Joists.AsNoTracking().ToArrayAsync()
                };
                
                return View(model); 
            }
        }

        [HttpGet]
        public IActionResult Test() {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken] // CSRF token check. Token is encoded inside form
        public async Task<IActionResult> Calculator(int? bid, int? jid) {
            if (bid == null || jid == null) {
                return NotFound();
            }

            using (_db) {
                // May require ordering by bl.Length to ensure array ordering.

                var model = new BoardLengthModel {
                    Board = await _db.Boards
                            .AsNoTracking()
                            .Include(b => b.BoardLengths)
                            .ThenInclude(bl => bl.Length)
                            .FirstOrDefaultAsync(x => x.BoardId == bid),

                    Joist = await _db.Joists
                            .AsNoTracking()
                            .Include(j => j.JoistLengths)
                            .ThenInclude(jl => jl.Length)
                            .FirstOrDefaultAsync(x => x.JoistId == jid)
                };

                if (model == null) {
                    return NotFound();
                }
                
                return View(model);
            }
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error() {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
