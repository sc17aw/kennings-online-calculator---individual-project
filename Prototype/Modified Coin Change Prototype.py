from math import ceil
from collections import Counter


def calculate_area(width, height):
    return round(width * height, 2)


def round_decimal(decimal, digits):
    return ceil(decimal * 10**digits) / 10**digits


def calculate_joists(width, size):
    print("------------------------------------")
    print("JOIST SIZE      | {}mm".format(size))

    width = width * 1000
    joists = int(ceil(width / size)) + 1

    display_spacings(joists, size, width)


def display_spacings(joists, size, width):
    print("JOISTS REQUIRED | {}".format(joists))
    print("WIDTH(mm)       | {}mm\n".format(int(width)))

    for i in range(0, joists-1):
        print("{}mm".format(i * size))

    print("{}mm".format(int(width)))


def calculate_extras(area):
    print("------------------------------------\nEXTRAS:")
    print("  SCREW BOXES   = {} boxes".format(ceil(area / 5)))
    print("  PAINT COVERS  = {} covers".format(ceil(area / 16)))
    print("  WEED MEMBRANE = {} m squared\n\n".format(ceil(area)))


def trim_board_lengths(boards, joist):
    return [board for board in boards if not board*1000 % joist]


def get_available_boards(joist):
    board_lengths = [1.5, 1.8, 2.4, 3.0, 3.6, 3.9, 4.2, 4.5, 4.8, 5.1, 5.4]
    print("OLD BOARDS:", board_lengths)
    board_lengths = trim_board_lengths(board_lengths, joist)
    print("NEW BOARDS:", board_lengths)

    remove = 999

    while(len(board_lengths) != 1 and remove != 0):
        for i in range(0, len(board_lengths)):
            print("{}. {}".format(i+1, board_lengths[i]))

        remove = int(input("\nEnter value to remove. 0 to escape "))

        if remove > 0 and remove <= len(board_lengths):
            print("Removed {} from selection\n".format(
                                                    board_lengths[remove-1]
                                                    ))
            del board_lengths[remove-1]

    board_lengths = [0.1, 0.2] + board_lengths
    return board_lengths


def get_optimal_boards(boards, length):
    def calculate_boards(curr_length):
        curr_board = None
        for board in boards:
            if board == curr_length:
                return board
            if board < curr_length:
                curr_board = board

        curr_length = round(curr_length - curr_board, 2)
        return [curr_board] + [calculate_boards(curr_length)]

    # Credit to Kindall https://stackoverflow.com/questions/10823877/what-is-the-fastest-way-to-flatten-arbitrarily-nested-lists-in-python
    def flatten(items):
        for i, x in enumerate(items):
            while i < len(items) and isinstance(items[i], list):
                items[i:i+1] = items[i]

        return items

    results = calculate_boards(length)

    if type(results) == float:
        return [results]
    else:
        return list(flatten(list(results)))


def display_optimal_boards(boards):
    board_dict = dict(Counter(boards))

    for board, quantity in board_dict.items():
        print("  {}m: {} board/s".format(board, quantity))


def calculate_efficient_boards(boards, length):
    results = get_optimal_boards(boards, length)

    remainder = round(results.count(0.2)*0.2 + results.count(0.1)*0.1, 2)
    results = [board for board in results if board != 0.2 and board != 0.1]

    if remainder > 0 and len(results) > 0:
        if results[-1] == boards[-1]:
            results.append(boards[2])

        results, waste = find_optimal_overflow(results,
                                               boards,
                                               remainder,
                                               length)

        if waste > 0:
            results, waste = trim_overflow(results, boards, waste)

        display_results(length, results, waste)

    else:
        if len(results) == 0:
            results.append(boards[2])
            waste = round(results[0] - length, 2)
            display_results(length, results, waste)

        else:
            display_optimal_boards(results)
            print("  Perfect fit found, no waste")


def display_results(length, results, waste):
    print("------------------------------------")
    print("RESULTS FOR A {}MTR LENGTH:".format(length))
    display_optimal_boards(results)
    print("  -----------------\n  WASTE: {}m".format(waste))


def find_optimal_overflow(results, boards, remainder, length):
    tail = boards.index(results[-1])
    for board in range(tail, len(boards)):
        if boards[board] - (results[-1] + remainder) > 0:
            results[-1] = boards[board]
            waste = round(sum(results) - length, 2)
            return results, waste

        else:
            waste = round(sum(results) - length, 2)
            if waste > 0:
                return results, waste

    results.append(boards[2])
    find_optimal_overflow(results, boards, remainder, length)


def trim_overflow(results, boards, waste):
    for result in range(len(results)-1, len(results)-3, -1):
        if waste == 0:
            break

        tail = boards.index(results[result])

        for board in range(tail-1, 1, -1):
            diff = round(results[result] - boards[board], 2)

            if diff <= waste:
                results[result] = boards[board]
                waste = round(waste - diff, 2)

    return results, waste


# MAIN
if __name__ == "__main__":
    print("------------------------------------")

    length = float(input("Enter the length              "))
    width = float(input("Enter the width               "))
    joist_size = int(input("Enter the joist type/size(mm) "))

    area = calculate_area(width, length)
    area = round_decimal(area, 2)

    calculate_joists(width, joist_size)
    calculate_extras(area)

    available_boards = get_available_boards(joist_size)

    calculate_efficient_boards(available_boards, length)
