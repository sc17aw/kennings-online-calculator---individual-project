from collections import Counter


def get_optimal_boards(input_boards: list, length: float):
    boards = input_boards

    def calculate_boards(curr_length):
        curr_board = None
        for board in boards:
            if board == curr_length:
                return board
            if board < curr_length:
                curr_board = board

        curr_length = round(curr_length - curr_board, 2)
        return [curr_board] + [calculate_boards(curr_length)]

    # Credit to Kindall https://stackoverflow.com/questions/10823877/what-is-the-fastest-way-to-flatten-arbitrarily-nested-lists-in-python
    def flatten(items):
        for i, x in enumerate(items):
            while i < len(items) and isinstance(items[i], list):
                items[i:i+1] = items[i]

        return items

    results = calculate_boards(length)
    return list(flatten(results))


def display_optimal_boards(boards):
    board_dict = dict(Counter(boards))

    for board, quantity in board_dict.items():
        print(" {}m: {} board/s".format(board, quantity))


def calculate_efficient_boards(boards, length):
    results = get_optimal_boards(boards, target_length)

    remainder = round(results.count(0.2)*0.2 + results.count(0.1)*0.1, 2)

    results = [board for board in results if board != 0.2 and board != 0.1]

    if remainder > 0:
        if results[-1] == boards[-1]:
            results.append(boards[2])

        results, waste = find_optimal_overflow(results, boards, remainder)

        if waste > 0:
            results, waste = trim_overflow(results, boards, waste)

            print("------------------------------------")
            print("RESULTS FOR A {}MTR AREA:".format(length))
            display_optimal_boards(results)
            print("  WASTE: {}m".format(waste))

    else:
        display_optimal_boards(results)
        print(" Perfect fit found, no waste")


def find_optimal_overflow(results, boards, remainder):
    tail = boards.index(results[-1])

    for board in range(tail, len(boards)):
        if boards[board] - (results[-1] + remainder) > 0:
            results[-1] = boards[board]
            waste = round(sum(results) - target_length, 2)

            return results, waste

    results.append(boards[2])
    find_optimal_overflow(results, boards, remainder)


def trim_overflow(results, boards, waste):
    for result in range(len(results)-1, len(results)-3, -1):
        if waste == 0:
            break

        tail = boards.index(results[result])

        for board in range(tail-1, 1, -1):
            diff = round(results[result] - boards[board], 2)

            if diff <= waste:
                results[result] = boards[board]
                waste = round(waste - diff, 2)

    return results, waste


boards = [0.1, 0.2, 1.5, 1.8, 2.4, 3.0, 3.6, 3.9, 4.2, 4.5, 4.8, 5.1, 5.4]
target_length = 7.5

calculate_efficient_boards(boards, target_length)

"""
    for result in range(len(results)-1, len(results)-3, -1):
      tail = boards.index(results[result])
      for board in range(tail-1, 1, -1):
        diff = round(results[result] - boards[board], 2)

        if waste == 0:
          break

        if diff <= waste:
          results[result] = boards[board]
          waste = round(waste - diff, 2)
"""
"""
    for result in range(len(results)-2, len(results)):
      tail = boards.index(results[-result])
      print("ENTERED SPACE OF {}".format(results[result]))
      for board in range(tail-1, 1, -1):
        diff = round(results[-result] - boards[board], 2)
        if waste == 0:
          break

        if diff <= waste:
          print("REPLACE {} with {}".format(results[-result], boards[board]))
          results[-result] = boards[board]
          waste = round(waste - diff, 2)
          print("DIFF: {} WASTE: {}".format(diff, waste))
"""
